# 👾 Juego _Terraforming Mars_ 👾

Desarrollo del juego _Terraforming Mars_ (versión simplificada) el cual consiste en la creación de un tablero con una cierta cantidad de casillas y el uso de los dados que darán valores aleatorios para poder avanzar durante la partida. Además, a cada jugador se le asignará una corporación. 

Al tirar los dados, según el valor obtenido de estos, se asocia a la corporación de un jugador ciertos tipos de casillas y/o también se modifican las condicioens medioambientales. Una vez se cumplen ciertas condiciones, el juego finaliza y gana la corporación que tenga más puntos. Estos puntos se proporcionarán según las casillas que haya conseguido durante la partida.

## 📄 Descripción
Proyecto realizado para la asignatura "Acceso a datos" desarrollado durante el segundo año de Grado Superior de Desarrollo de Aplicaciones Multiplataforma + Perfil videojuegos y ocio digital (DAMvi).

## 💻 Tecnologías
![Hibernate](https://img.shields.io/badge/Hibernate-59666C?style=for-the-badge&logo=Hibernate&logoColor=white)
![SpringBoot](https://img.shields.io/badge/spring-%236DB33F.svg?style=for-the-badge&logo=spring&logoColor=white)
![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=openjdk&logoColor=white)
![Apache Maven](https://img.shields.io/badge/Apache%20Maven-C71A36?style=for-the-badge&logo=Apache%20Maven&logoColor=white)
![MySQL](https://img.shields.io/badge/mysql-4479A1.svg?style=for-the-badge&logo=mysql&logoColor=white)

## ⚙️ Funcionamiento
El funcionamiento se puede ver en el documento de pruebas del proyecto adjuntado a continuación: [Documento de pruebas](https://gitlab.com/slimmyteam/accesodatos/pojectehibernatedaospring/-/blob/main/Document_de_proves_Hibernate.pdf).

## ©️ Desarrolladores
- Ethan Corchero Martín.
- Ana María Gómez León.
- Selene Milanés Rodríguez.