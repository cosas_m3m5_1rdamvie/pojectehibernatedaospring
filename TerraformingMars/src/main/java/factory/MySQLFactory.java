package factory;

import dao.CorporationsDAOMySQL;
import dao.GamesDAOMySQL;
import dao.GamesWinnerDAOMySQL;
import dao.IGenericDAO;
import dao.MakersDAOMySQL;
import dao.PlayersDAOMySQL;

public class MySQLFactory implements DAOFactory<IGenericDAO> {

	public IGenericDAO create(String daoType) {
		if ("gameswinner".equalsIgnoreCase(daoType)) {
			return new GamesWinnerDAOMySQL();
		} else if ("players".equalsIgnoreCase(daoType)) {
			return new PlayersDAOMySQL();
		} else if ("games".equalsIgnoreCase(daoType)) {
			return new GamesDAOMySQL();
		} else if ("corporations".equalsIgnoreCase(daoType)) {
			return new CorporationsDAOMySQL();
		} else if ("makers".equalsIgnoreCase(daoType)) {
			return new MakersDAOMySQL();
		}
		return null;
	}

}
