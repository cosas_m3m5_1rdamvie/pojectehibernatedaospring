package entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Games")
public class Games implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Idgame", updatable = false, insertable = false)
	private int idGame;
	
	@Column(name = "Oxygen")
	private int oxygen = 0;
	
	@Column(name = "Temperature")
	private int temperature = -30;
	
	@Column(name = "Oceans")
	private int oceans = 0;
	
	@Column(name = "Datestart")
	private LocalDateTime dateStart;

	@Column(name = "Dateend")
	private LocalDateTime dateEnd;

	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "games", fetch = FetchType.EAGER) 
	private Set<Players> players = new HashSet<Players>();
	
	@OneToOne
	@JoinColumn(name="idGamesWinner")
	private GamesWinner gamesWinner;

	public Games() {
		super();
		this.players = new HashSet<Players>();
	}

	public Games(int oxygen, int temperature, int oceans, LocalDateTime dateStart, LocalDateTime dateEnd) {
		this(dateStart, dateEnd);
		this.oxygen = oxygen;
		this.temperature = temperature;
		this.oceans = oceans;
	}

	public Games(LocalDateTime dateStart, LocalDateTime dateEnd) {
		this();
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
	}

	public int getOxygen() {
		return oxygen;
	}

	public void setOxygen(int oxygen) {
		this.oxygen = oxygen;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public int getOceans() {
		return oceans;
	}

	public void setOceans(int oceans) {
		this.oceans = oceans;
	}

	public LocalDateTime getDateStart() {
		return dateStart;
	}

	public void setDateStart(LocalDateTime dateStart) {
		this.dateStart = dateStart;
	}

	public LocalDateTime getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(LocalDateTime dateEnd) {
		this.dateEnd = dateEnd;
	}	
	
	public Set<Players> getPlayers() {
		return players;
	}

	public void setPlayers(Set<Players> players) {
		this.players = players;
	}

	public GamesWinner getGamesWinner() {
		return gamesWinner;
	}

	public void setGamesWinner(GamesWinner gamesWinner) {
		this.gamesWinner = gamesWinner;
	}

	public int getIdGame() {
		return idGame;
	}

	
	
	@Override
	public String toString() {
		return "Games [idGame=" + idGame + ", oxygen=" + oxygen + ", temperature=" + temperature + ", oceans=" + oceans
				+ ", dateStart=" + dateStart + ", dateEnd=" + dateEnd + ", players=" + players + ", gamesWinner="
				+ gamesWinner + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idGame;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Games other = (Games) obj;
		if (idGame != other.idGame)
			return false;
		return true;
	}	
	
	
}
