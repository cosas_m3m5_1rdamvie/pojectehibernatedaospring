package entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Corporations")
public class Corporations implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Idcorporations", updatable = false, insertable = false)
	private int idCorporations;

	@Column(name = "Name", length = 20, nullable = false)
	private String name;

	@Column(name = "Description", length = 50, nullable = false)
	private String description;

	@Column(name = "Victorypoints")
	private int victoryPoints = 0; // valor per defecte

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "corporation")
	private Players player;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "corporation")
	private Set<Makers> makers;

	public Corporations() {
		super();
		this.makers = new HashSet<Makers>();
	}

	public Corporations(String name, String description, int victoryPoints) {
		this();
		this.name = name;
		this.description = description;
		this.victoryPoints = victoryPoints;
	}
	
	public Corporations(String name, String description) {
		this();
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getVictoryPoints() {
		return victoryPoints;
	}

	public void setVictoryPoints(int victoryPoints) {
		this.victoryPoints = victoryPoints;
	}

	public Players getPlayer() {
		return player;
	}

	public void setPlayer(Players player) {
		this.player = player;
	}

	public Set<Makers> getMakers() {
		return makers;
	}

	public void setMakers(Set<Makers> makers) {
		this.makers = makers;
	}

	public int getIdCorporations() {
		return idCorporations;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idCorporations;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Corporations other = (Corporations) obj;
		if (idCorporations != other.idCorporations)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Corporations [idCorporations=" + idCorporations + ", name=" + name + ", description=" + description
				+ ", victoryPoints=" + victoryPoints + ", makers=" + makers + "]";
	}

}
