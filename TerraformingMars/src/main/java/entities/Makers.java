package entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Makers")
public class Makers implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Idmakers", updatable = false, insertable = false)
	private int idMakers;

	@Column(name = "Name", length = 20, nullable = false)
	private String name;

	@Column(name = "Maxneighbours")
	private int maxNeighbours = 0; // valor per defecte

	@Enumerated(EnumType.STRING)
	@Column(name = "Typemaker")
	private TypeMaker typeMaker;

	@ManyToOne
	@JoinColumn(name = "IdCorporation(FK)")
	private Corporations corporation;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "Makers_NeighbourMaker", joinColumns = @JoinColumn(name = "IdNeighbourMaker"), inverseJoinColumns = @JoinColumn(name = "IdMaker"))
	private Set<Makers> neighbourMakers;

	public Makers() {
		super();
		this.neighbourMakers = new HashSet<Makers>();
	}

	public Makers(String name, TypeMaker typeMaker) {
		this();
		this.name = name;
		this.typeMaker = typeMaker;
	}

	public Makers(String name, TypeMaker typeMaker, int maxNeighbours) {
		this(name, typeMaker);
		this.maxNeighbours = maxNeighbours;
	}

	public int getIdMakers() {
		return idMakers;
	}

	public void setIdMakers(int idMakers) {
		this.idMakers = idMakers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxNeighbours() {
		return maxNeighbours;
	}

	public void setMaxNeighbours(int maxNeighbours) {
		this.maxNeighbours = maxNeighbours;
	}

	public TypeMaker getTypeMaker() {
		return typeMaker;
	}

	public void setTypeMaker(TypeMaker typeMaker) {
		this.typeMaker = typeMaker;
	}

	public Corporations getCorporation() {
		return corporation;
	}

	public void setCorporation(Corporations corporation) {
		this.corporation = corporation;
	}

	public Set<Makers> getNeighbourMakers() {
		return neighbourMakers;
	}

	public void setNeighbourMakers(Set<Makers> neighbourMakers) {
		this.neighbourMakers = neighbourMakers;
	}

	@Override
	public String toString() {
		return "Makers [idMakers=" + idMakers + ", name=" + name + ", maxNeighbours=" + maxNeighbours + ", typeMaker=" + typeMaker + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idMakers;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Makers other = (Makers) obj;
		if (idMakers != other.idMakers)
			return false;
		return true;
	}

}
