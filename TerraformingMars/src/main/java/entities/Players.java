package entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Players")
public class Players implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Idplayer", updatable = false, insertable = false)
	private int idPlayer;
	
	@Column(name = "Name", length = 50, nullable = false)
	private String name;
	
	@Column(name = "Wins")
	private int wins = 0;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="playersgames")
	private Set<Games> games;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="idCorporation")
	private Corporations corporation;	

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "player")
	private Set<GamesWinner> gamesWinner;

	public Players() {
		super();
		this.games = new HashSet<Games>();
		this.gamesWinner = new HashSet<GamesWinner>();
	}

	public Players(String name, int wins) {
		this();
		this.name = name;
		this.wins = wins;
	}	
	

	public Players(String name) {
		this();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWins() {
		return wins;
	}

	public void setWins(int wins) {
		this.wins = wins;
	}

	public Set<Games> getGames() {
		return games;
	}

	public void setGames(Set<Games> games) {
		this.games = games;
	}

	public Corporations getCorporation() {
		return corporation;
	}

	public void setCorporation(Corporations corporation) {
		this.corporation = corporation;
	}

	public Set<GamesWinner> getGamesWinner() {
		return gamesWinner;
	}

	public void setGamesWinner(Set<GamesWinner> gamesWinner) {
		this.gamesWinner = gamesWinner;
	}

	public int getIdPlayer() {
		return idPlayer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idPlayer;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Players other = (Players) obj;
		if (idPlayer != other.idPlayer)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Players [idPlayer=" + idPlayer + ", name=" + name + ", wins=" + wins + ", games=" + games
				+ ", corporation=" + corporation + ", gamesWinner=" + gamesWinner + "]";
	}
		
	
}
