package main;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

import dao.ICorporationsDAO;
import dao.IGamesDAO;
import dao.IGamesWinnerDAO;
import dao.IMakersDAO;
import dao.IPlayersDAO;
import entities.Corporations;
import entities.Games;
import entities.GamesWinner;
import entities.Makers;
import entities.Players;
import entities.TypeMaker;
import factory.DAOFactory;
import factory.DAOFactoryImpl;

public class MainClass {

	static DAOFactoryImpl fDAO = new DAOFactoryImpl();
	static DAOFactory<?> mySQLDAO = DAOFactoryImpl.getFactory("mysql");
	static IMakersDAO makersDAO = (IMakersDAO) mySQLDAO.create("makers");
	static IPlayersDAO playersDAO = (IPlayersDAO) mySQLDAO.create("players");
	static ICorporationsDAO corporationsDAO = (ICorporationsDAO) mySQLDAO.create("corporations");
	static IGamesDAO gamesDAO = (IGamesDAO) mySQLDAO.create("games");
	static IGamesWinnerDAO gamesWinnerDAO = (IGamesWinnerDAO) mySQLDAO.create("gameswinner");
	static Random r = new Random();
	static Deque<Players> colaJugadores = new LinkedList<Players>();
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("GENERANDO BASE DE DATOS...");

		// --------------------- [[ CREAMOS LOS PLAYERS ]] ---------------------//
		Players p1 = new Players("Player1");
		Players p2 = new Players("Player2");
		Players p3 = new Players("Player3");
		Players p4 = new Players("Player4");

		playersDAO.create(p1);
		playersDAO.create(p2);
		playersDAO.create(p3);
		playersDAO.create(p4);

		// --------------------- [[ CREAMOS LAS CORPORACIONES ]] ---------------------//
		Corporations c1 = new Corporations("Thorgate", "Description of Thorgate");
		Corporations c2 = new Corporations("Mining Guild", "Description of Mining Guild");
		Corporations c3 = new Corporations("U.N. Mars Initiative", "Description of U.N. Mars Initiative");
		Corporations c4 = new Corporations("Ecoline", "Description of Ecoline");
		Corporations c5 = new Corporations("Helion", "Description of Helion");
		Corporations c6 = new Corporations("Credicor", "Description of Credicor");
		Corporations c7 = new Corporations("Phoblog", "Description of Phoblog");
		Corporations c8 = new Corporations("Tharsis Republic", "Description of Tharsis Republic");

		corporationsDAO.create(c1);
		corporationsDAO.create(c2);
		corporationsDAO.create(c3);
		corporationsDAO.create(c4);
		corporationsDAO.create(c5);
		corporationsDAO.create(c6);
		corporationsDAO.create(c7);
		corporationsDAO.create(c8);

		// --------------------- [[ PARTIDA ]] ---------------------//
		boolean jugando = true;
		while (jugando) {
			System.out.println("\n Selecciona una de las opciones: " + "\n - 1. Jugar." + "\n - 2. Salir del juego.");

			switch (sc.nextInt()) {
			case 1:
				// CREANDO EL TABLERO
				VaciarTablaMakers();
				VaciarVictoryPoints();

				// --------------------- [[ CREACION DE MAKERS ]] ---------------------//
				for (int i = 0; i < 9; i++) {
					Makers m1 = new Makers("Ocea" + i, TypeMaker.OCEA);
					makersDAO.create(m1);
				}
				for (int i = 0; i < 26; i++) {
					Makers m1 = new Makers("Bosc" + i, TypeMaker.BOSC);
					makersDAO.create(m1);
				}
				for (int i = 0; i < 26; i++) {
					Makers m1 = new Makers("Ciutat" + i, TypeMaker.CIUTAT);
					makersDAO.create(m1);
				}

				// --------------------- [[ ASIGNAMOS NEIGHBOURS A LOS MAKERS ]] ---------------------//
				AsignarNeighbours();

				// --------------------- [[ CREACION DE GAME Y GAMESWINNER ]] ---------------------//
				Games g1 = new Games();
				gamesDAO.create(g1);

				GamesWinner gw = new GamesWinner(LocalDateTime.now());
				gamesWinnerDAO.create(gw);
				g1.setGamesWinner(gw);
				gamesDAO.update(g1);

				g1.setDateStart(LocalDateTime.now());
				gamesDAO.update(g1);

				p1.getGames().add(g1);
				p2.getGames().add(g1);
				p3.getGames().add(g1);
				p4.getGames().add(g1);

				// --------------------- [[ ASIGNAMOS CORPORACIONES ALEATORIAMENTE A JUGADORES ]] ---------------------//
				AsignarCorporacionesAPlayers();

				playersDAO.update(p1);
				playersDAO.update(p2);
				playersDAO.update(p3);
				playersDAO.update(p4);

				System.out.println("\n*************BASE DE DATOS CREADA*************\n");

				// --------------------- [[ ASIGNAMOS TURNO A JUGADORES ]] ---------------------//
				AsignarPlayersTurno();
				boolean partida = true;

				while (partida) {

					// --------------------- [[ TIRADA DE DADOS ]] ---------------------//
					TirarDados(g1);

					// --------------------- [[ COMPROBACION DE INDICADORES ]] ---------------------//
					partida = ComprobarIndicadores(g1);

					// --------------------- [[ CAMBIAMOS TURNO ]] ---------------------//
					if (partida)
						giraColaJugadors();
				}

				// --------------------- [[ FINALIZAR PARTIDA ]] ---------------------//
				FinalitzarPartida();

				// --------------------- [[ RESOLUCIO GUANYADOR ]] ---------------------//
				Players playerGuanyador = ResolucioDeGuanyador();
				gw.setPlayer(playerGuanyador);
				gw.setDateEnd(LocalDateTime.now());
				gamesWinnerDAO.update(gw);
				g1.setDateEnd(LocalDateTime.now());
				gamesDAO.update(g1);
				break;

			case 2:
				System.out.println("*************SALIMOS DEL JUEGO*************");
				jugando = false;
				break;

			default:
				System.out.println("Pulsa 1 o 2 para escoger");
				break;
			}
		}
	}

	private static void VaciarTablaMakers() {
		List<Makers> listaMakersRecuperados = makersDAO.findAll();
//		makersDAO.limpiarTabla("makers_neighbourmaker");
//		makersDAO.limpiarTabla("makers");
		if (!listaMakersRecuperados.isEmpty()) {
			for (Makers maker : listaMakersRecuperados) {
				makersDAO.delete(maker);
			}
		}
	}

	private static void VaciarVictoryPoints() {
		List<Corporations> listaCorporaciones = corporationsDAO.findAll();
		for (Corporations corporations : listaCorporaciones) {
			corporations.setVictoryPoints(0);
			corporationsDAO.update(corporations);
		}
	}

	private static void AsignarNeighbours() {
		List<Makers> listaMakersRecuperados = makersDAO.findAll();

		for (int i = 0; i < 4; i++) {
			Makers makerRecuperado = listaMakersRecuperados.get(r.nextInt(listaMakersRecuperados.size()));
			if (makerRecuperado.getMaxNeighbours() != 0)
				i--;
			else {
				makerRecuperado.setMaxNeighbours(3);
				makersDAO.update(makerRecuperado);
			}
		}

		for (int i = 0; i < 20; i++) {
			Makers makerRecuperado = listaMakersRecuperados.get(r.nextInt(listaMakersRecuperados.size()));
			if (makerRecuperado.getMaxNeighbours() != 0)
				i--;
			else {
				makerRecuperado.setMaxNeighbours(4);
				makersDAO.update(makerRecuperado);
			}
		}

		for (Makers maker : listaMakersRecuperados) {
			if (maker.getMaxNeighbours() == 0) {
				maker.setMaxNeighbours(6);
				makersDAO.update(maker);
			}
		}

		listaMakersRecuperados = makersDAO.findAll();

		for (Makers maker : listaMakersRecuperados) {
			int contador = 0;
			while (maker.getMaxNeighbours() > maker.getNeighbourMakers().size()) {
				Makers makerRandom = listaMakersRecuperados.get(r.nextInt(listaMakersRecuperados.size()));
				contador++;
				if (contador > 1000) {
					break;
				}
				if ((maker != makerRandom) && (!maker.getNeighbourMakers().contains(makerRandom)) && (!makerRandom.getNeighbourMakers().contains(maker))
						&& (makerRandom.getMaxNeighbours() > makerRandom.getNeighbourMakers().size())) {
					maker.getNeighbourMakers().add(makerRandom);
					makerRandom.getNeighbourMakers().add(maker);
					makersDAO.update(makerRandom);
				}
				makersDAO.update(maker);
			}
		}

		// Makers que no han terminado
		ArrayList<Makers> makersSinAsignar = new ArrayList<Makers>();

		for (Makers maker : listaMakersRecuperados) {
			if (maker.getMaxNeighbours() > maker.getNeighbourMakers().size())
				makersSinAsignar.add(maker);
		}

		// Reasignar casillas
		for (Makers maker : makersSinAsignar) { // Makers sin asignar
			for (Makers makerRecuperado : listaMakersRecuperados) { // Todos los makers
				if (maker != makerRecuperado && !makerRecuperado.getNeighbourMakers().contains(maker) && maker.getMaxNeighbours() > maker.getNeighbourMakers().size()) {
					// Si el otro maker no es el inicial, el otro maker no contiene el maker inicial
					// y el maker inicial no tiene el máximo de neighbours
					int random = r.nextInt(makerRecuperado.getNeighbourMakers().size());
					ArrayList<Makers> neighbours = new ArrayList<Makers>();
					neighbours.addAll(makerRecuperado.getNeighbourMakers());
					Makers makerRandom = neighbours.get(random);
					// Se elige un neighbour random

					if (!maker.getNeighbourMakers().contains(makerRandom)) {
						// Si el maker inicial no contiene el neighbour random
						maker.getNeighbourMakers().add(makerRandom); // Se lo queda
						makerRecuperado.getNeighbourMakers().remove(makerRandom);

						// El segundo maker borra el neighbour de la lista y se queda con el maker
						// inicial como neighbour
						makerRecuperado.getNeighbourMakers().add(maker);

						// Update base de datos :D
						makersDAO.update(makerRecuperado);
						makersDAO.update(maker);
					}
				}
			}
		}
	}

	private static void AsignarCorporacionesAPlayers() {
		List<Players> players = playersDAO.findAll();

		VaciarCorporacionesPlayers(players);

		List<Corporations> corporations = corporationsDAO.findAll();
		System.out.println("*************ASIGNACIÓN DE CORPORACIONES*************");
		for (Players player : players) {
			int random = r.nextInt(corporations.size());
			player.setCorporation(corporations.get(random));
			System.out.println("Al " + player.getName() + " le ha tocado la corporación " + corporations.get(random).getName() + ".");
			corporations.remove(random);
		}
	}

	private static void VaciarCorporacionesPlayers(List<Players> players) {
		for (Players player : players) {
			player.setCorporation(null);
			playersDAO.update(player);
		}

	}

	private static void AsignarPlayersTurno() {
		List<Players> players = playersDAO.findAll();
		int tamany = players.size();
		for (int i = 0; i < tamany; i++) {
			int random = r.nextInt(players.size());
			colaJugadores.addLast(players.get(random));
			players.remove(random);
		}
		System.out.println("Jugadores: " + colaJugadores);
		System.out.println("Es el turno de: ##" + colaJugadores.getFirst().getName());
	}

	private static void TirarDados(Games g) {
		int contTemp = 0;
		int contOxi = 0;
		int contOcea = 0;
		int contBosc = 0;
		int contCiutat = 0;
		int contPunts = 0;
		// hay que comprobar si la lista está vacía para lanzar un mensaje que diga que
		// no quedan más casillas para asignar corporations
		for (int i = 0; i < 6; i++) {
			int dado = r.nextInt(1, 7);
			System.out.println("Dado: " + dado);
			switch (dado) {
			case 1:
				contTemp++;
				if (contTemp == 3) {
					g.setTemperature(g.getTemperature() + 2);
					System.out.println("Aumentamos la temperatura 2ºC.");
					contTemp = 0;
					gamesDAO.update(g);
				}
				break;
			case 2:
				contOxi++;
				if (contOxi == 3) {
					g.setOxygen(g.getOxygen() + 1);
					System.out.println("Aumentamos el oxígeno en 1.");
					contOxi = 0;
					gamesDAO.update(g);
				}
				break;
			case 3:
				contOcea++;
				if (contOcea == 3) {
					ArrayList<Makers> listaMakersOceans = BuscarOceans();
					if (!listaMakersOceans.isEmpty()) {
						Makers m = listaMakersOceans.get(r.nextInt(listaMakersOceans.size()));
						m.setCorporation(colaJugadores.getFirst().getCorporation());
						makersDAO.update(m);
						Corporations c = colaJugadores.getFirst().getCorporation();
						c.getMakers().add(m);
						corporationsDAO.update(c);
						System.out.println("La " + colaJugadores.getFirst().getCorporation().getName() + " del " + colaJugadores.getFirst().getName() + " ha ganado un maker de océano.");
						contOcea = 0;
					}
				}
				break;
			case 4:
				contBosc++;
				if (contBosc == 4) {
					List<Makers> listaMakers = makersDAO.findAll();
					ArrayList<Makers> listaMakersBosc = new ArrayList<Makers>();
					for (Makers maker : listaMakers) {
						if (maker.getTypeMaker() == TypeMaker.BOSC && maker.getCorporation() == null) {
							listaMakersBosc.add(maker);
						}
					}
					if (listaMakersBosc.isEmpty()) {
						System.out.println("No quedan makers sin corporaciones!!!");
					} else {
						Makers m = listaMakersBosc.get(r.nextInt(listaMakersBosc.size()));
						m.setCorporation(colaJugadores.getFirst().getCorporation());
						makersDAO.update(m);
						Corporations c = colaJugadores.getFirst().getCorporation();
						c.getMakers().add(m);
						corporationsDAO.update(c);
						System.out.println("La " + colaJugadores.getFirst().getCorporation().getName() + " del " + colaJugadores.getFirst().getName() + " ha ganado un maker de bosque.");
						contBosc = 0;
					}
				}
				break;
			case 5:
				contCiutat++;
				if (contCiutat == 3) {
					List<Makers> listaMakers = makersDAO.findAll();
					ArrayList<Makers> listaMakersCiutat = new ArrayList<Makers>();
					for (Makers maker : listaMakers) {
						if (maker.getTypeMaker() == TypeMaker.CIUTAT && maker.getCorporation() == null) {
							listaMakersCiutat.add(maker);
						}
					}
					if (listaMakersCiutat.isEmpty()) {
						System.out.println("No quedan makers sin corporaciones!!!");
					} else {
						Makers m = listaMakersCiutat.get(r.nextInt(listaMakersCiutat.size()));
						m.setCorporation(colaJugadores.getFirst().getCorporation());
						makersDAO.update(m);
						Corporations c = colaJugadores.getFirst().getCorporation();
						c.getMakers().add(m);
						corporationsDAO.update(c);
						System.out.println("La " + colaJugadores.getFirst().getCorporation().getName() + " del " + colaJugadores.getFirst().getName() + " ha ganado un maker de ciudad.");
						contCiutat = 0;
					}
				}
				break;
			case 6:
				contPunts++;
				if (contPunts == 3) {
					colaJugadores.getFirst().getCorporation().setVictoryPoints(colaJugadores.getFirst().getCorporation().getVictoryPoints() + 2);
					contPunts = 0;
					corporationsDAO.update(colaJugadores.getFirst().getCorporation());
					System.out.println("La " + colaJugadores.getFirst().getCorporation().getName() + " del " + colaJugadores.getFirst().getName() + " ha ganado 2 puntos.");
				}
				break;
			}
		}
		System.out.println("@@TEMPERATURA: " + g.getTemperature());
		System.out.println("@@OXÍGENO: " + g.getOxygen());
	}

	private static boolean ComprobarIndicadores(Games g) {
		int contCondicions = 0;

		if (g.getTemperature() >= 0) {
			System.out.println("Se ha cumplido la condición: la temperatura es igual o mayor a 0.");
			contCondicions++;
		}
		if (g.getOxygen() >= 14) {
			System.out.println("Se ha cumplido la condición: el oxígeno es igual o mayor a 14.");
			contCondicions++;
		}
		ArrayList<Makers> listaMakersOceans = BuscarOceans();

		if (listaMakersOceans == null) {
			System.out.println("Se ha cumplido la condición: todos los oceanos tienen un jugador asignado.");
			contCondicions++;
		}

		if (contCondicions >= 2) {
			System.out.println("Se han cumplido 2 condiciones.");
			System.out.println("\n*************FIN DE LA PARTIDA*************\n");
			return false; // Parar de jugar
		} else
			return true; // Seguir jugando
	}

	private static ArrayList<Makers> BuscarOceans() {
		List<Makers> listaMakers = makersDAO.findAll();
		ArrayList<Makers> listaMakersOceans = new ArrayList<Makers>();
		for (Makers maker : listaMakers) {
			if (maker.getTypeMaker() == TypeMaker.OCEA && maker.getCorporation() == null) {
				listaMakersOceans.add(maker);
			}
		}
		if (listaMakersOceans.isEmpty()) {
			System.out.println("No quedan makers sin corporaciones!!!");
		}
		return listaMakersOceans;
	}

	private static void giraColaJugadors() {
		colaJugadores.addLast(colaJugadores.pop());
		System.out.println("Es el turno de: ##" + colaJugadores.getFirst().getName());
	}

	private static void FinalitzarPartida() {
		Players p1 = playersDAO.get(1);
		Players p2 = playersDAO.get(2);
		Players p3 = playersDAO.get(3);
		Players p4 = playersDAO.get(4);

		System.out.println("\n*************RECUENTO DE PUNTOS*************");
		CalcularPunts(p1);
		CalcularPunts(p2);
		CalcularPunts(p3);
		CalcularPunts(p4);

		int ciutatJugador1 = getMakersByTypeCorporation(p1.getCorporation(), TypeMaker.CIUTAT);
		int ciutatJugador2 = getMakersByTypeCorporation(p2.getCorporation(), TypeMaker.CIUTAT);
		int ciutatJugador3 = getMakersByTypeCorporation(p3.getCorporation(), TypeMaker.CIUTAT);
		int ciutatJugador4 = getMakersByTypeCorporation(p4.getCorporation(), TypeMaker.CIUTAT);

		int boscsJugador1 = getMakersByTypeCorporation(p1.getCorporation(), TypeMaker.BOSC);
		int boscsJugador2 = getMakersByTypeCorporation(p2.getCorporation(), TypeMaker.BOSC);
		int boscsJugador3 = getMakersByTypeCorporation(p3.getCorporation(), TypeMaker.BOSC);
		int boscsJugador4 = getMakersByTypeCorporation(p4.getCorporation(), TypeMaker.BOSC);

		// Comparator para ordenar los puntos
		Comparator<Integer> comparatorPunts = new Comparator<Integer>() {

			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}
		};

		ArrayList<Integer> listaCiutatsPunts = new ArrayList<Integer>();
		listaCiutatsPunts.add(ciutatJugador1);
		listaCiutatsPunts.add(ciutatJugador2);
		listaCiutatsPunts.add(ciutatJugador3);
		listaCiutatsPunts.add(ciutatJugador4);

		listaCiutatsPunts.sort(comparatorPunts);
		// System.out.println("@@listaCiutatsPunts (después): " + listaCiutatsPunts);

		ArrayList<Integer> listaBoscPunts = new ArrayList<Integer>();
		listaBoscPunts.add(boscsJugador1);
		listaBoscPunts.add(boscsJugador2);
		listaBoscPunts.add(boscsJugador3);
		listaBoscPunts.add(boscsJugador4);
		listaBoscPunts.sort(comparatorPunts);
		// System.out.println("@@listaBoscPunts (después): " + listaBoscPunts);

		List<Integer> sublistCiutatsPunts = listaCiutatsPunts.subList(0, 2);
		List<Integer> sublistBoscPunts = listaBoscPunts.subList(0, 2);

		asignarFirstSecondVictoryPoints(sublistCiutatsPunts, TypeMaker.CIUTAT);
		asignarFirstSecondVictoryPoints(sublistBoscPunts, TypeMaker.BOSC);

		puntosFinales();

	}

	private static void CalcularPunts(Players p) { // Puntos de corporacion y oceano
		Set<Makers> listaMakers = p.getCorporation().getMakers();
		int punts = 0;
		int oceans = 0;

		for (Makers maker : listaMakers) {
			if (maker.getTypeMaker() == TypeMaker.OCEA)
				oceans++;
		}

		punts += p.getCorporation().getMakers().size();
		System.out.println("La corporación " + p.getCorporation().getName() + " del " + p.getName() + " ha conseguido durante la partida " + p.getCorporation().getVictoryPoints() + " puntos.");
		System.out.println("La corporación " + p.getCorporation().getName() + " del " + p.getName() + " suma " + p.getCorporation().getMakers().size() + " puntos por las corporaciones conseguidas.");

		if (oceans >= 3) {
			punts += 3;
			System.out.println("La corporación " + p.getCorporation().getName() + " del " + p.getName() + " suma 3 puntos por conseguir corporaciones de tipo océano.");
		}
		p.getCorporation().setVictoryPoints(p.getCorporation().getVictoryPoints() + punts);
		System.out.println("La corporación " + p.getCorporation().getName() + " del " + p.getName() + " hasta ahora tiene " + p.getCorporation().getVictoryPoints() + " puntos.\n");
	}

	private static int getMakersByTypeCorporation(Corporations corporation, TypeMaker type) {
		int makers = 0;

		for (Makers maker : corporation.getMakers()) {
			if (maker.getTypeMaker() == type) {
				makers++;
			}
		}
		return makers;
	}

	private static void asignarFirstSecondVictoryPoints(List<Integer> sublist, TypeMaker type) {
		List<Corporations> corporacions = corporationsDAO.findAll();
		while (!sublist.isEmpty()) {
			for (Corporations corp : corporacions) {
				if (!sublist.isEmpty() && sublist.get(0) == getMakersByTypeCorporation(corp, type)) {
					if (sublist.get(0) == 0) {
						return;
					} else if (sublist.size() == 2) {
						corp.setVictoryPoints(corp.getVictoryPoints() + 5);
						System.out.println("La corporación " + corp.getName() + " consigue 5 puntos por ser la primera en tener más makers de " + type + ".");
					} else {
						corp.setVictoryPoints(corp.getVictoryPoints() + 3);
						System.out.println("La corporación " + corp.getName() + " consigue 3 puntos por ser la segunda en tener más makers de " + type + "");
					}
					sublist.remove(0);
				}
			}
		}
	}

	private static void puntosFinales() {
		System.out.println("\n*************PUNTOS FINALES*************");
		List<Players> players = playersDAO.findAll();
		for (Players p : players) {
			System.out.println("La corporación " + p.getCorporation().getName() + " del " + p.getName() + " finaliza la partida con " + p.getCorporation().getVictoryPoints() + " puntos.");
		}
	}

	private static Players ResolucioDeGuanyador() {
		List<Corporations> corporacions = corporationsDAO.findAll();
		Comparator<Corporations> comparator = new Comparator<Corporations>() {
			public int compare(Corporations c1, Corporations c2) {
				return c2.getVictoryPoints() - c1.getVictoryPoints();
			}
		};
		corporacions.sort(comparator);

		List<Players> players = playersDAO.findAll();
		for (Players player : players) {
			if (player.getCorporation() == corporacions.get(0)) {
				player.setWins(player.getWins() + 1);
				playersDAO.update(player);
				System.out.println("\n*************GANADOR*************");
				System.out
						.println("Ha ganado el " + player.getName() + " con la corporación " + player.getCorporation().getName() + " y con " + player.getCorporation().getVictoryPoints() + " puntos.");
				return player;
			}
		}
		return null;
	}
}
