package utils;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class Utils {

	static EntityManager entityManager;
	static EntityManagerFactory factory;

	public static synchronized EntityManager getCurrentEntityManager() {

		if (factory == null) {
			factory = Persistence.createEntityManagerFactory("mars");
		}
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}
		return entityManager;
	}

	public static void close() {
		entityManager.clear();
		entityManager.close();
		factory.close();
	}

}
