package dao;

import java.io.Serializable;
import java.util.List;

public interface IGenericDAO <T,ID extends Serializable>{
	T get (ID id);
	void create (T entity);
	void update (T entity);
	void delete (T entity);
	void limpiarTabla(String tablaName);
	List<T> findAll();
	
	public Class<T> getEntityClass();
}
