package dao;

import java.io.Serializable;

import entities.Makers;

public interface IMakersDAO extends IGenericDAO<Makers, Integer>, Serializable{

}
