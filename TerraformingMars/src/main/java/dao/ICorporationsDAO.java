package dao;

import java.io.Serializable;

import entities.Corporations;


public interface ICorporationsDAO extends IGenericDAO<Corporations, Integer>, Serializable{

}
