package dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.HibernateException;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import utils.Utils;

public class GenericDAOMySQL<T, ID extends Serializable> implements IGenericDAO<T, ID> {

	EntityManager entityManager;

	public GenericDAOMySQL() {
		this.entityManager = Utils.getCurrentEntityManager();
	}

	public T get(ID id) {
		try {
			entityManager.getTransaction().begin();
			T entity = (T) entityManager.find(getEntityClass(), id);
			entityManager.getTransaction().commit();
			// entityManager.close();
			return entity;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (entityManager != null && entityManager.getTransaction() != null) {
				System.out.println("\n......Transaction Is Being Rolled Back.......");
				entityManager.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		return null;
	}

	public void create(T entity) {
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(entity);
			entityManager.getTransaction().commit();

		} catch (HibernateException e) {
			entityManager.close();
			e.printStackTrace();
			if (entityManager != null && entityManager.getTransaction() != null) {
				System.out.println("\n......Transaction Is Being Rolled Back.......");
				entityManager.getTransaction().rollback();
			}
			e.printStackTrace();
		}
	}

	public void update(T entity) {
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(entity);
			entityManager.getTransaction().commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			if (entityManager != null && entityManager.getTransaction() != null) {
				System.out.println("\n......Transaction Is Being Rolled Back.......");
				entityManager.getTransaction().rollback();
			}
			e.printStackTrace();
		}
	}

	public void delete(T entity) {
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(entity);
			entityManager.getTransaction().commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			if (entityManager != null && entityManager.getTransaction() != null) {
				System.out.println("\n......Transaction Is Being Rolled Back.......");
				entityManager.getTransaction().rollback();
			}
			e.printStackTrace();
		}
	}

	public List<T> findAll() {
		try {
			entityManager.getTransaction().begin();
			Query query = entityManager.createQuery("Select e from " + getEntityClass().getName() + " e");
			List<T> entities = query.getResultList();
			entityManager.getTransaction().commit();
			return entities;

		} catch (HibernateException e) {
			e.printStackTrace();
			if (entityManager != null && entityManager.getTransaction() != null) {
				System.out.println("\n......Transaction Is Being Rolled Back.......");
				entityManager.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		return null;
	}

	public Class<T> getEntityClass() {
		return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public void limpiarTabla(String tablaName) {
		try {
			entityManager.getTransaction().begin();
			/*Query query = entityManager.createQuery("set SQL_SAFE_UPDATES = 0;");*/
			Query query2 = entityManager.createQuery("DELETE FROM "+tablaName);
			entityManager.getTransaction().commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			if (entityManager != null && entityManager.getTransaction() != null) {
				System.out.println("\n......Transaction Is Being Rolled Back.......");
				entityManager.getTransaction().rollback();
			}
			e.printStackTrace();
		}
	}
}
