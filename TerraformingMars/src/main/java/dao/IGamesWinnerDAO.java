package dao;

import java.io.Serializable;

import entities.GamesWinner;

public interface IGamesWinnerDAO extends IGenericDAO<GamesWinner, Integer>, Serializable {

}
